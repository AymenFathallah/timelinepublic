package tn.esprit.spring;

import static org.junit.Assert.assertEquals;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import tn.esprit.spring.entities.Contrat;
import tn.esprit.spring.services.IContratService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ContratServiceImplTest {


	@Autowired 
	IContratService ct; 

	@Test
	public void testAddUser() throws ParseException {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date d = dateFormat.parse("2015-03-23");
		Contrat c = new Contrat(d, "CDI", 1500); 
		Contrat contratAdded = ct.addContrat(c); 
		assertEquals(c.getTypeContrat(), contratAdded.getTypeContrat());
	}

	@Test
	public void testRetrieveAllContrats() {
		List<Contrat> listContrats = ct.retrieveAllContrats(); 
		assertEquals(3, listContrats.size());
	}

	@Test
	public void testRetrieveContrat() {
		Contrat contratRetrieved = ct.retrieveContrat(1); 
		assertEquals(1, contratRetrieved.getReference());
	}
}
