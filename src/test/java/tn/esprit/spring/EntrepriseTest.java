package tn.esprit.spring;

import static org.junit.Assert.assertEquals;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.tomcat.jni.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import tn.esprit.spring.entities.Entreprise;
import tn.esprit.spring.entities.Role;
import tn.esprit.spring.services.EntrepriseServiceImpllogs;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EntrepriseTest {
	
	@Autowired 
	EntrepriseServiceImpllogs en; 

	@Test
	public void testAddEntreprise() throws ParseException {
		
		Entreprise ent = new Entreprise("amine", "commerciale"); 
		Entreprise EntrepriseAdded = en.ajouterEntreprise(ent);
		assertEquals(ent.getName(), ent.getRaisonSocial());
	}


	@Test
	public void testgetAllEntreprise() {
		List<Entreprise> entreprises =en.getAllEntreprise();
		// if there are 1 users in DB : 
		assertEquals(1, entreprises.size());
	} 

	@Test
	public void testgetEntrepriseById() {
		Entreprise entreprise = en.getEntrepriseById(1); 
		assertEquals(1L, entreprise.getId());
	}


}

