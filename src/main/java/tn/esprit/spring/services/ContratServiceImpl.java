package tn.esprit.spring.services;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import tn.esprit.spring.entities.Contrat;
import tn.esprit.spring.repository.ContratRepository;

public class ContratServiceImpl implements IContratService{
	@Autowired
	ContratRepository contratRepository;

	private static final Logger l = LogManager.getLogger(ContratServiceImpl.class);
 
	@Override
	public List<Contrat> retrieveAllContrats() {
		l.info("In  retrieveAllContrats : "); 
		List<Contrat> contrats = (List<Contrat>) contratRepository.findAll();  
		for (Contrat contrat : contrats) {
			l.debug("contrat +++ : " + contrat);
		}
		l.info("Out of retrieveAllContrats."); 
		return contrats;
	}

	@Override
	public Contrat addContrat(Contrat c) {
		l.info("In  addContrat : " + c); 
		Contrat contratSaved = contratRepository.save(c);
		l.info("Out of  addContrat. "); 
		return contratSaved; 
	}

	@Override
	public void deleteContrat(int reference) {
		contratRepository.deleteById(reference);
		l.info("contrat with refenrece = " + reference + "deleted");
	}

	@Override
	public Contrat retrieveContrat(int reference) {
		l.info("in  retrieveContrat reference = " + reference);
		Contrat c =  contratRepository.findById(reference).orElse(null);
		l.info("contrat returned : " + c);
		return c; 
	}
}
