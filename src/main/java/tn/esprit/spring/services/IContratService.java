package tn.esprit.spring.services;

import java.util.List;

import tn.esprit.spring.entities.Contrat;

public interface IContratService {

	public List<Contrat> retrieveAllContrats();
	public Contrat addContrat(Contrat c);
	public void deleteContrat(int reference);
	public Contrat retrieveContrat(int reference);
}
