package tn.esprit.spring.services;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tn.esprit.spring.entities.Departement;
import tn.esprit.spring.entities.Entreprise;
import tn.esprit.spring.repository.EntrepriseRepository;


public class EntrepriseServiceImpllogs implements IEntrepriseService{

	@Autowired
	EntrepriseRepository entrepriseRepository;
	

	private static final Logger l = LogManager.getLogger(EntrepriseServiceImpl.class);
 
	@Override
	public List<Entreprise> getAllEntreprise() {
		l.info("In  allentreprises : "); 
		List<Entreprise> entreprises = (List<Entreprise>) entrepriseRepository.findAll();  
		for (Entreprise entreprise : entreprises) {
			l.debug("entreprise +++ : " + entreprise);
		}
		l.info("Out of allentreprises."); 
		return entreprises;
	}

	@Override
	public Entreprise ajouterEntreprise (Entreprise e) {
		l.info("In  addEntreprise : " + e); 
		Entreprise EntrepriseSaved = entrepriseRepository.save(e);
		l.info("Out of  addEntreprise "); 
		return EntrepriseSaved; 
	}


	@Override
	public void deleteEntrepriseById(int id) {
		entrepriseRepository.deleteById(id);
		l.info("entreprise id deleted : " + id);
	}

	@Override
	public Entreprise getEntrepriseById(int id) {
		l.info("in  entreprise id = " + id);
		Entreprise e =  entrepriseRepository.findById(id).orElse(null);
		l.info("entreprise returned : " + e);
		return e; 
	}


	@Override
	public int ajouterDepartement(Departement dep) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void affecterDepartementAEntreprise(int depId, int entrepriseId) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<String> getAllDepartementsNamesByEntreprise(int entrepriseId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteDepartementById(int depId) {
		// TODO Auto-generated method stub
		
	}

}